/**
 * BÀI1: TÍNH TIỀN LƯƠNG NHÂN VIÊN
 * input: lương 1 ngày và số ngày làm
 * output:
 */


 function tinhTongLuong() {
    var luongMotNgay = document.getElementById("luongMotNgay").value*1;
    var soNgayLam = document.getElementById("soNgayLam").value*1;
    var tienLuong = 0;
    tienLuong = luongMotNgay * soNgayLam;
    document.getElementById("tongLuong").innerHTML = tienLuong.toLocaleString();
}


/**
 * BÀI 2: TÍNH GIÁ TRỊ TRUNG BÌNH
 */

function tinhTrungBinh () {
    var soThuNhat = document.getElementById("soThuNhat").value*1;
    var soThuHai = document.getElementById("soThuHai").value*1;
    var soThuBa = document.getElementById("soThuBa").value*1;
    var soThuTu = document.getElementById("soThuTu").value*1;
    var soThuNam = document.getElementById("soThuNam").value*1;
    var soTrungBinh = 0;
    soTrungBinh = (soThuNhat + soThuHai + soThuBa + soThuTu + soThuNam) / 5;
    console.log('so trung binh', soTrungBinh)
    document.getElementById ("hienThi").innerHTML = soTrungBinh;

}



/**
 * BÀI 3: QUY ĐỔI TIỀN
 */

function quyDoiTien () {
    var tyGiaUsd = document.getElementById("giaUsd").value*1;
    var soTienUsd = document.getElementById("soTienUsd").value*1;
    var tongTien = 0;
    tongTien = tyGiaUsd * soTienUsd;
    document.getElementById("hienThiTien").innerHTML = tongTien.toLocaleString();
}

/**
 * BÀI 4: TÍNH DIỆN TÍCH - CHU VI HÌNH CHỮ NHẬT
 */


function tinhChuViDienTich() {
    var chieuDai = document.getElementById ("chieuDai").value*1;
    var chieuRong = document.getElementById ("chieuRong").value*1;
    var chuVi = 0;
    var dienTich = 0;
    chuVi = (chieuDai + chieuRong)*2;
    dienTich = chieuDai * chieuRong;
    document.getElementById("hienThiDienTich").innerHTML = dienTich;
    document.getElementById("hienThiChuVi").innerHTML = chuVi;

}


/**
 * BÀI 5: TÍNH TỔNG 2 KÍ SỐ
 */

function tinhTongKySo() {
    var Num = document.getElementById("nhapSo").value*1;
    var soHangDonVi = Num % 10;
    var soHangChuc = Math.floor(Num/10);
    var result = 0;
    result = soHangChuc + soHangDonVi;
    document.getElementById("hienThiTong").innerHTML = result;
}
